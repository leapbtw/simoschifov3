# Configurazione

- crea la cartella **simoschifo/config**
- in questa cartella crea tre file: **channels.json**,  **config.json** e **vc_names.txt**

### channels.json
qui dovrai inserire i ```channel_id``` dei canali vocali che il bot userà al posto dei commenti, dentro le ""
```
{
	"CREA_VC_CHANNEL_ID" : "se entri in questo canale, il bot crea un nuovo canale vocale e ti ci sposta dentro",
	"CREA_VC_CATEGORY_ID" : "id della categoria di CREA_VC_CATEGORY_ID",
	"riparazioni" : "canale vocale a 8kbps, lo usiamo quando abbiamo internet che non funziona",
	"afk" : "canale vocale afk"
}
```

### config.json
il file di configurazione principale del bot
```
{
    "token" : "token del bot, lo trovi sul Developer Portal",
    "clientId" : "id utente del bot",
    "guildId" : "id del server discord"
}
```

### vc_names.txt
questo file contiene delle parole a caso che il bot selezionerà randomicamente per dare un nome ai canali vocali che crea.
mettici parole a caso separate da spazi
```Lampada Cavolfiore Pioggia Chitarra Montagna Scimmia Orizzonte Ananas Cioccolato Bicicletta```
