const { SlashCommandBuilder } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;

module.exports = {
	data: new SlashCommandBuilder()
		.setName('countdown')
		.setDescription('Countdown di N secondi')
		.addIntegerOption(option =>
			option
				.setName("secondi")
				.setDescription("numero di secondi da aspettare")
				.setRequired(true),
		),

	async execute(interaction) {
		const secondi = interaction.options.getInteger('secondi')
		await interaction.deferReply();

		for (let i = 0; i < secondi; i++) {
			await interaction.editReply((secondi - i).toString() + "...");
			await wait(1000)
		}
		return interaction.followUp("**Go!!**")
	},
}
