const { SlashCommandBuilder } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;

module.exports = {
	data: new SlashCommandBuilder()
		.setName('attachment')
		.setDescription('metti un allegato')
		.addAttachmentOption(option =>
			option
				.setName("allegato")
				.setDescription("l\'allegato")
				.setRequired(true),
		),

	async execute(interaction) {
		const attachment = interaction.options.getAttachment("allegato")

		const name = attachment.name
		const url = attachment.url
		const proxyURL = attachment.proxyURL
		return interaction.reply("nome: " + name + "\nurl: " + url + "\nproxyUrl: " + proxyURL)
	},
}
