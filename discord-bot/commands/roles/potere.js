const { SlashCommandBuilder } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;

module.exports = {
	data: new SlashCommandBuilder()
		.setName('potere')
		.setDescription('ti da il **POTERE**'),

	async execute(interaction) {
		await interaction.deferReply();

		await interaction.guild.roles.fetch()
		const role = interaction.guild.roles.cache.find(r => r.name === 'potere')
		await interaction.member.roles.add(role)

		const secondi = 30
		await interaction.editReply("ho dato il ruolo <@&" + role + "> a " + interaction.user.toString() + " [" + secondi + "s]")

		for (let i = 0; i < secondi; i++) {
			await interaction.editReply("ho dato il ruolo <@&" + role + "> a " + interaction.user.toString() + " [" + (secondi - i).toString() + "s]")
			await wait(1000)
		}
		await interaction.member.roles.remove(role)

		return interaction.editReply(interaction.user.toString() + " **è di nuovo un plebeo**")
	},
}
