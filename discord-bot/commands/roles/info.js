const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('info')
		.setDescription('ti da info sul tuo potere'),

	async execute(interaction) {
		await interaction.guild.roles.fetch()
		const memberRoles = interaction.guild.members.cache
			.get(interaction.member.id)
			.roles.cache.map(role => role.name);
		const filteredRoles = memberRoles.filter(role => role !== '@everyone');
		const executorRole = interaction.user.toString();
		const userAvatar = interaction.user.displayAvatarURL();
		const roleMentions = filteredRoles.map(roleName => {
			const role = interaction.guild.roles.cache.find(r => r.name === roleName);
			return role ? role.toString() : roleName;
		});

		// facciamo in modo che la risposta sia "personale" e non tagghi tutto il server per via delle menzioni dei ruoli <-- down
		await interaction.deferReply({ephemeral: true})
		return interaction.editReply(`Questo commando e' stato eseguito da ${executorRole}, ` +
			`che si e' unito il ${interaction.member.joinedAt} ai compagni di zio walrein.\n` +
			`I suoi ruoli attivi sono: \n${roleMentions.join('\n')} \n${userAvatar}`)

	},
}
