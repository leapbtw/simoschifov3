const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('calc')
		.setDescription('Esegui operazioni matematiche su una lista di numeri')
		.addStringOption(option =>
			option
				.setName('operazione')
				.setDescription('L\'operazione da eseguire (som, sot, mol, div)')
				.setRequired(true)
				.addChoices(
					{ name: 'Somma', value: 'som' },
					{ name: 'Sottrazione', value: 'sot' },
					{ name: 'Moltiplicazione', value: 'mol' },
					{ name: 'Divisione', value: 'div'}
				)
		)
		.addStringOption(option =>
			option
				.setName('numeri')
				.setDescription('La lista di numeri separati da virgola')
				.setRequired(true),
		),
		

	async execute(interaction) {
		const numeriInput = interaction.options.getString('numeri');
		const operazione = interaction.options.getString('operazione');
		
		// Split della stringa di numeri e conversione in array di numeri
		const numeriArray = numeriInput.split(',').map(Number);

		let risultato;
		switch (operazione) {
			case 'som':
				risultato = numeriArray.reduce((acc, num) => acc + num, 0);
				break;
			case 'sot':
				risultato = numeriArray.reduce((acc, num) => acc - num);
				break;
			case 'mol':
				risultato = numeriArray.reduce((acc, num) => acc * num, 1);
				break;
			case 'div':
				risultato = numeriArray.reduce((acc, num) => acc / num);
				break;
			default:
				return interaction.reply('Operazione non valida. Le opzioni valide sono: som, sot, mol, div');
		}

		return interaction.reply(`Il risultato dell'operazione ${operazione} è: ${risultato}`);
	},
};
