const { SlashCommandBuilder } = require('discord.js');
const wait = require('node:timers/promises').setTimeout;
const { guildId } = require('../../config/config.json');
const { CREA_VC_CHANNEL_ID } = require('../../config/channels.json')

module.exports = {
	cooldown: 120,
	data: new SlashCommandBuilder()
		.setName('smuta')
		.setDescription('Sposta @user per fargli sentire i ping e smutarlo')
		.addUserOption(option =>
			option
				.setName("utente")
				.setDescription("l\'utente da spostare")
				.setRequired(true),
		),

	async execute(interaction) {
		await interaction.deferReply();
		const guild = interaction.client.guilds.cache.get(guildId)
		// const idUtente = interaction.options.getUser('utente').id
		// const member = guild.members.cache.get(idUtente)
		const member = interaction.options.getMember('utente')
		
		if (member.voice.channel === null) return await interaction.editReply('L\'utente non è in chat vocale')		
		await interaction.editReply('Rompo le scatole a <@' + member + '>, magari si smuta')

		// se l'utente è in una voice chat...
		if (member.voice.channel) {
			const VCOriginale = member.voice.channelId
			// VCs è una lista di ID delle chat vocali
			const VCs = guild.channels.cache.filter((c) => c.bitrate !== null && c.type === 2).map(channel => channel.id);
			
			for (let i = 0; i < 5; i++) {
				let trovataNuovaVC = false;
				let prossimaVC = 0;
				// cerco una nuova vc che non sia quella in cui già è
				while (!trovataNuovaVC) {
					prossimaVC = VCs[Math.floor(Math.random() * VCs.length)];
					if (member.voice.channelId != prossimaVC 
						&& member.voice.channelId != CREA_VC_CHANNEL_ID)
							trovataNuovaVC = true;
				}
				// sposto la persona
				await member.voice.setChannel(prossimaVC)
				await wait(1500)
			}
			// risposto il disperato nella chat vocale in cui era prima
			await member.voice.setChannel(VCOriginale)
		}
	},
}
