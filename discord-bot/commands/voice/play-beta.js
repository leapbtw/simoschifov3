const { useMainPlayer, QueryType } = require('discord-player');
const { SlashCommandBuilder } = require('discord.js');
const { AttachmentExtractor } = require('@discord-player/extractor');

const { exec } = require('child_process');
const fs = require('fs');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('play-beta')
        .setDescription('Comando /play in beta, funziona con youtube')
        .addStringOption(option =>
            option
                .setName("query")
                .setDescription("Canzone da cercare")
                .setRequired(true),
        ),

    async execute(interaction) {
        const player = useMainPlayer();
        player.extractors.register(AttachmentExtractor);
        const channel = interaction.member.voice.channel;
        if (!channel) return interaction.reply('You are not connected to a voice channel!');
        
        let canzone = interaction.options.getString("query");
        canzone = canzone.replace(/ +(?= )/g, '');

        await interaction.deferReply();
        
        const file_name = canzone.replace(/ /g, '_').replace(/[^a-zA-Z_]/g, '') + "-" + Math.floor(Math.random() * 10000) + ".mp3";
        const comando = `yt-dlp -v -x --audio-format mp3 -f 'bestaudio[ext=m4a]' "ytsearch:${canzone}" -o ${file_name}`
        console.log(`canzone: ${canzone}\tfile_name: ${file_name}\ncomando: ${comando}`);
        
        exec(comando, async (error, stdout, stderr) => {
            if (error) {
                console.error(`Error executing command: ${error}`);
                console.log(`Stdout: ${error}`);
                return interaction.followUp('Something went wrong while downloading the song.');
            }

            if (fs.existsSync("./" + file_name)) {
                console.log("iniziamo a fare play");
                try {
                    const { track } = await player.play(channel, "./" + file_name, {
                        searchEngine: QueryType.FILE,
                        nodeOptions: {
                            metadata: interaction,
                        },
                    });
                    
                    // Listen to the 'emptyQueue' event on the player
                   player.events.on('emptyQueue', (queue, track) => {
                        console.log(`Track ${canzone} has ended.`);
                        if (fs.existsSync("./" + file_name)) {
                            fs.unlink(`./${file_name}`, (err) => {
                                if (err) console.error(`Error deleting file: ${err.message}`);
                                else console.log(`File ${file_name} deleted successfully.`);
                            });
                        }
                    });


                    return interaction.followUp(`**${canzone}** enqueued!`);
                } catch (e) {
                    return interaction.followUp(`Something went wrong: ${e}`);
                }
            } else {
                console.log(`File ${file_name} not found.`);
                return interaction.followUp('Could not find the downloaded song file.');
            }
        });
    },
};

