const { useMainPlayer, QueryType } = require('discord-player');
const { SlashCommandBuilder } = require('discord.js');
const { AttachmentExtractor } = require('@discord-player/extractor');
const wait = require('node:timers/promises').setTimeout;

const tts = require('./tts/custom_tts_library.js');
const fs = require('fs');

module.exports = {
    data: new SlashCommandBuilder()
        .setName('tts')
        .setDescription('Dice quello che scrivi nella chat vocale')
        .addStringOption(option =>
            option
                .setName("query")
                .setDescription("Testo da parlare")
                .setRequired(true),
        ),

    async execute(interaction) {
        const player = useMainPlayer();
        player.extractors.register(AttachmentExtractor);
        const channel = interaction.member.voice.channel;
        if (!channel) return interaction.reply('You are not connected to a voice channel!');

        let text_to_speak = interaction.options.getString("query");
        text_to_speak = text_to_speak.replace(/ +(?= )/g, '');

        await interaction.deferReply();
        const file_name = text_to_speak.replace(/ /g, '_').replace(/[^a-zA-Z_]/g, '') + "-" + Math.floor(Math.random() * 10000) + ".mp3";

        // Create a Promise to handle the TTS file download
        await new Promise((resolve, reject) => {
            tts.getMp3(text_to_speak, function(err, data) {
                if (err) {
                    console.log(err);
                    reject(err);
                    return interaction.followUp('Something went wrong while generating the TTS audio.');
                }

                const file = fs.createWriteStream(file_name);
                file.write(data);
                file.end();

                // Resolve the promise when the file has been written
                file.on('finish', resolve);
                file.on('error', reject);
            });
        });

        // Now that the file is downloaded, we can play it
        try {
            const { track } = await player.play(channel, "./" + file_name, {
                searchEngine: QueryType.FILE,
                nodeOptions: {
                    metadata: interaction,
                },
            });

            // Listen to the 'trackEnd' event to delete the file after playback
            player.events.on('emptyQueue', (queue, track) => {
                console.log(`Track ${text_to_speak} has ended.`);
                if (fs.existsSync("./" + file_name)) {
                    fs.unlink(`./${file_name}`, (err) => {
                        if (err) console.error(`Error deleting file: ${err.message}`);
                        else console.log(`File ${file_name} deleted successfully.`);
                    });
                }
            });

            return interaction.followUp(`**${text_to_speak}** enqueued!`);
        } catch (e) {
            return interaction.followUp(`Something went wrong: ${e}`);
        }
    },
};

