const { SlashCommandBuilder, PermissionsBitField, ChannelType, Role } = require('discord.js');
const { guildId } = require('../../config/config.json');
const { CREA_VC_CHANNEL_ID, CREA_VC_CATEGORY_ID, riparazioni, afk, GSG } = require('../../config/channels.json')

const fs = require('node:fs');
const path = require('node:path');

const wait = require('node:timers/promises').setTimeout;

/**
 * allora qua il problema è che sarebbe carino se si potesse definire il tipo di argomento che si vuole (es. ruolo/utente)
 * e chiederne come input più di uno, mettendoli tutti in qualche tipo di struttura dati (lista, che ne so).
 * il punto è che non si può (almeno credo?), quindi 1 input === 1 variabile. Ho scelto arbitrariamente 2 ruoli e 5 utenti massimo
 */
module.exports = {
	cooldown: 20,
	data: new SlashCommandBuilder()
		.setName('vc-personale')
		.setDescription('Tagga i Ruoli (max 2) e/o Utenti (max 5) che avranno accesso alla VC.')
		.addMentionableOption(option =>
			option
				.setName("menziona_utente_o_ruolo1")
				.setDescription("Menziona un ruolo/utente")
				.setRequired(false),
		).addMentionableOption(option =>
			option
				.setName("menziona_utente_o_ruolo2")
				.setDescription("Menziona un ruolo/utente")
				.setRequired(false),
		).addMentionableOption(option =>
			option
				.setName("menziona_utente_o_ruolo3")
				.setDescription("Menziona un ruolo/utente")
				.setRequired(false),
		).addMentionableOption(option =>
			option
				.setName("menziona_utente_o_ruolo4")
				.setDescription("Menziona un ruolo/utente")
				.setRequired(false),
		).addMentionableOption(option =>
			option
				.setName("menziona_utente_o_ruolo5")
				.setDescription("Menziona un ruolo/utente")
				.setRequired(false),
		),

	async execute(interaction) {
		await interaction.deferReply({ephemeral: true});
		
		// prendiamo gli input e li mettiamo nelle loro variabili (Sigh.)
		const perm1 = interaction.options.getMentionable('menziona_utente_o_ruolo1')
		const perm2 = interaction.options.getMentionable('menziona_utente_o_ruolo2')
		const perm3 = interaction.options.getMentionable('menziona_utente_o_ruolo3')
		const perm4 = interaction.options.getMentionable('menziona_utente_o_ruolo4')
		const perm5 = interaction.options.getMentionable('menziona_utente_o_ruolo5')
		
		const member = interaction.member
		const guild = interaction.client.guilds.cache.get(guildId)

		// qui stessa logica di events/voiceStateUpdate.js, ma aggiungiamo dei permessi alla VC creata
		const words = fs.readFileSync('./config/vc_names.txt', 'utf-8');
		const wordsArray = words.split(' ');
		const randomIndex = Math.floor(Math.random() * wordsArray.length);
		const randomWord = wordsArray[randomIndex];

		const newChannel = await guild.channels.create({
			name: "VCpvt #" + randomWord,
			type: ChannelType.GuildVoice,
			parent: CREA_VC_CATEGORY_ID,
			position: guild.channels.cache.get(CREA_VC_CHANNEL_ID).rawPosition + 1,

			// di default il canale è visibile SOLO a chi lo crea
			permissionOverwrites: [
				{
					id: interaction.guild.id,
					deny: [PermissionsBitField.Flags.ViewChannel],
				},
				{
					id: member,
					allow: [PermissionsBitField.Flags.ViewChannel],
				},
			],
		});
		
		// estraggo i ruoli/utenti dall'input. Sicuramente c'è un modo migliore ma sono stanco
		if (perm1) newChannel.permissionOverwrites.create(perm1, { ViewChannel: true })
		if (perm2) newChannel.permissionOverwrites.create(perm2, { ViewChannel: true })
		if (perm3) newChannel.permissionOverwrites.create(perm3, { ViewChannel: true })
		if (perm4) newChannel.permissionOverwrites.create(perm4, { ViewChannel: true })
		if (perm5) newChannel.permissionOverwrites.create(perm5, { ViewChannel: true })
		
		if (member.voice.channelId) {
			// creo una risposta, ma siccome alcuni valori risultano nulli, pulisco la stringa con regex
			let reply = 'Ho creato il canale <#' + newChannel + '>!\nPossono accederci: '
				+ '<@' + (perm1 instanceof Role ? ('&' + perm1) : perm1) + '> <@' 
				+ (perm2 instanceof Role ? ('&' + perm2) : perm2) + '> <@' 
				+ (perm3 instanceof Role ? ('&' + perm3) : perm3) + '> <@' 
				+ (perm4 instanceof Role ? ('&' + perm4) : perm4) + '> <@' 
				+ (perm5 instanceof Role ? ('&' + perm5) : perm5) + '>.'
			interaction.editReply(reply.replace(new RegExp(' <@null>', 'g'), '').replace(new RegExp(' <@&null>', 'g'), ''))

			// sposto l'utente nella nuova voicechat
			return await member.voice.setChannel(newChannel)

		}

		// se non posso spostare l'utente dentro la VC perché non è già in una VC diversa, aspetto 30s e nel caso cancello la VC
		else interaction.editReply('Non sei connesso ad un canale vocale!\nNon posso spostarti dentro <#' + newChannel + '>.'
						+ '\nConnettiti entro 30s o ti riempio la casa di TNT. (╯°□°)╯︵ ┻━┻')

		await wait (30 * 1000)
		if (newChannel.members.size === 0) {
			interaction.editReply('_pssss..._')
			await wait(1500)
			interaction.editReply('**KABOOOOOOOM**')
			newChannel.delete('nessuno si è connesso alla VC')
		}
	},
}
