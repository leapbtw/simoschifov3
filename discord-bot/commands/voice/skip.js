const { useMainPlayer, useQueue } = require('discord-player');
const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('skip')
		.setDescription('skippa una song'),

	async execute(interaction) {
		const player = useMainPlayer();
		const channel = interaction.member.voice.channel;
		if (!channel) return interaction.reply('You are not connected to a voice channel!'); // make sure we have a voice channel
		const queue = useQueue(interaction.guild.id);
		queue.node.skip()
		return interaction.reply('Canzone skippata!');
	}
}
