const { SlashCommandBuilder } = require('discord.js');
const { useQueue } = require('discord-player');
const { guildId } = require('../../config/config.json');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('disconnect')
		.setDescription('Disconnette il bot dalla VC'),

  async execute(interaction) {
    const queue = useQueue(interaction.guild.id);
    
    // If there's no queue, it means the bot is not connected to a voice channel
    if (!queue) {
        return interaction.reply({ content: `I am **not** in a voice channel`, ephemeral: true });
    }
    
    // If we reach here, that means the bot is in a voice channel and we can safely delete the queue
    queue.delete();
    
    await interaction.reply('Mi sono disconnesso dalla VC!');
  },
};
