const { useMainPlayer, useQueue } = require('discord-player');
const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('volume')
		.setDescription('cambia il volume del bot')
		.addStringOption(option =>
			option
				.setName("valore")
				.setDescription('volume del bot')
				.setRequired(true)
		),

	async execute(interaction) {
		const player = useMainPlayer();
		const channel = interaction.member.voice.channel;
		if (!channel) return interaction.reply('You are not connected to a voice channel!'); // make sure we have a voice channel
		const vol = Number(interaction.options.getString('valore')); // we need input/query to play

		const queue = useQueue(interaction.guild.id);
		queue.node.setVolume(vol); //Pass the value for the volume here
		return interaction.reply('Volume impostato a ' + vol.toString() + "!")
	}
}
