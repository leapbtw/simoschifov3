const { SlashCommandBuilder } = require('discord.js');
//risponde pong
module.exports = {
	data: new SlashCommandBuilder()
		.setName('help')
		.setDescription('Lista dei comandi del bot'),
	async execute(interaction) {
		await interaction.reply(
			'** LISTA COMANDI BOT **' +
			'\n- /p | /play <player> <titolo canzone/file>' +
			'\n\tRiproduce musica.' +
			'\n- /volume <N>' +
			'\n\tCambia il volume del bot PER TUTTI.\n\tCambialo per te stesso! [default: 100].' +
			'\n- /tts <frase>' +
			'\n\tIl bot pronuncerà quella frase. Siccome leo non sa programmare,\n\ti tts finiscono in coda se c\'è una canzone in riproduzione.' +
			'\n- /smuta <persona>' +
			'\n\tSposta la persona tagga in giro per il server nella speranza che si smuti.\n\tNon abusarne troppo!' +
			'\n- /ping | /latency' +
			'\n\tPer vedere se il bot è online.' +
			'\n- /vc-personale <utenti/ruoli> ...' +
			'\n\tCrea una VC personale per te e chi tagghi, gli altri non possono vederla.' +
			'\n- /potere' +
			'\n\tTi da il **POTERE**.' +
			'\n- /disconnect' +
			'\n\tDisconnette il bot dalla VC.' +
			'\n- /skip' +
			'\n\tSkippa una delle canzoni in coda.' +
			'\n\n\n\n' +
			'\nALTRI COMANDI:' +
			'\n/server /info /user /random /countdown' +
			'' +
			'' +
			'' +
			'' +
			'' +
			'' 
		);
	},
};
