const { useMainPlayer, QueryType } = require('discord-player');
const { SlashCommandBuilder } = require('discord.js');
const { AttachmentExtractor } = require('@discord-player/extractor');
const wait = require('node:timers/promises').setTimeout;

module.exports = {
	data: new SlashCommandBuilder()
	.setName('faded')
	.setDescription('audio test')
    ,

		async execute(interaction) {
			// definiamo il player che vogliamo usare e il canale vocale in cui entrare
			const player = useMainPlayer();
			player.extractors.register(AttachmentExtractor)
			const channel = interaction.member.voice.channel;
			if (!channel) return interaction.reply('You are not connected to a voice channel!');
      
      // ci mettiamo tanto tempo quindi questo serve
			await interaction.deferReply();
      
			const file_name = "faded.mp3"

			// diamo il tempo all'mp3 di scaricarsi dai server di google translate
			await wait(1500)
			try {
        console.log("iniziamo a fare play");
				const { track } = await player.play(channel, "./commands/utility/" + file_name, {
					searchEngine: QueryType.FILE,
					nodeOptions: {
						metadata: interaction,
					},
				});

				return interaction.followUp(`**Faded - AUDIO TEST** enqueued!`);
			} catch (e) {
				// let's return error if something failed
				return interaction.followUp(`Something went wrong: ${e}`);
			}
		},
	}
