const { SlashCommandBuilder } = require('discord.js');
const axios = require('axios');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('ai')
    .setDescription('Ask a question to the AI model')
    .addStringOption(option =>
        option.setName('model')
          .setDescription('Modello. (numero più alto == più intelligente, più lento)')
          .setRequired(true)
          .addChoices(
            { name: '135M', value: 'smollm:135m' },
            { name: '360M', value: 'smollm:360m' },
            { name: '1.7B', value: 'smollm:1.7b' },
            { name: '2B', value: 'gemma2:2b' }
          )
    )
    .addStringOption(option =>
      option.setName('prompt')
        .setDescription('The question to ask')
        .setRequired(true)
    ),

  async execute(interaction) {
    await interaction.deferReply();

    const prompt = interaction.options.getString('prompt');
    const model = interaction.options.getString('model');
    console.log("pronpt:\t" + prompt);
    let fullResponse = '';

    try {
      // Make a request to the AI model
      const response = await axios({
        method: 'POST',
        url: 'http://localhost:11434/api/generate',
        data: {
          model: model,
          prompt: prompt
        },
        responseType: 'stream'
      });

      let currentMessage = '';

      // Handle streaming response
      response.data.on('data', chunk => {
        const lines = chunk.toString().split('\n').filter(line => line.trim() !== '');
        for (const line of lines) {
          if (fullResponse.length > 1950) {
            interaction.editReply({ content: fullResponse + "`2000 CHAR LIMIT HIT`"});
            break;
          }
          try {
            const json = JSON.parse(line);
            if (json.response) {
              fullResponse += json.response;
              interaction.editReply({ content: fullResponse });
            }
            if (json.done) {
              break;
            }
          } catch (error) {
            console.error('Error parsing JSON:', error);
          }
        }
      });

      response.data.on('end', () => {
        console.log('Streaming ended');
      });

    } catch (error) {
      console.error('Error:', error);
      await interaction.editReply('There was an error processing your request.');
    }
  },
};

