const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('random')
		.setDescription('random da 1 a max')
		.addStringOption(option =>
			option
				.setName('max')
				.setDescription('max')
				.setRequired(true)
		),

	async execute(interaction) {
		// genero un numero random
		await interaction.reply((Math.floor(Math.random() * Number(interaction.options.getString('max'))) + 1).toString());
	},
};
