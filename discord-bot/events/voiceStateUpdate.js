const { Events, ChannelType } = require('discord.js');

const { guildId } = require('../config/config.json');
const { clientId } = require('../config/config.json');
const { CREA_VC_CHANNEL_ID, CREA_VC_CATEGORY_ID, riparazioni, afk } = require('../config/channels.json')

const fs = require('fs');
const path = require('path');
const wait = require('node:timers/promises').setTimeout;

module.exports = {
	name: Events.VoiceStateUpdate,
	async execute(oldState, newState) {
				// if (oldState.channel === null) console.log("si è appena connesso!")
                // else if (newState.channel === null) console.log("si è disconnesso")
                // else if (oldState.channel != newState.channel) console.log("si è spostato!")
		
		const guild = newState.client.guilds.cache.get(guildId)
                
		// creo una chat vocale quando qualcuno entra in #CREA-VOICECHAT
		if (newState.channelId === CREA_VC_CHANNEL_ID) {
			// leggo words.txt e scelgo una parola a caso come nome della nuova voice-chat
			// (è più facile dire "entra in lobby zoo" piuttosto che "entra in lobby 395048")
			const words = fs.readFileSync('./config/vc_names.txt', 'utf-8');
			const wordsArray = words.split(' ');
			const randomIndex = Math.floor(Math.random() * wordsArray.length);
			const randomWord = wordsArray[randomIndex];

			const newChannel = await guild.channels.create({
			    name: "VC #" + randomWord,
			    type: ChannelType.GuildVoice,
			    parent: CREA_VC_CATEGORY_ID,
			    position: guild.channels.cache.get(CREA_VC_CHANNEL_ID).rawPosition + 1,
			    // your permission overwrites or other options here
			});
			
			// spostiamo il canale esattamente sotto al canale CREA-VC, e ci spostiamo l'utente dentro
			// guild.channels.setPosition(newChannel, newState.rawPosition + 1)
			newState.setChannel(newChannel)
		}
		

		if (newState.channel === null || (oldState.channel != null && oldState.channel != newState.channel)) {
			// controllo se NON è una VC che fa parte del nostro gruppo, nel caso ignoro
			if (oldState.channel.parentId != CREA_VC_CATEGORY_ID || oldState.channelId === CREA_VC_CHANNEL_ID ||
				oldState.channelId === riparazioni || oldState.channelId === afk ) {
				return
			}

			// guardo se c'è qualcuno dentro, se non c'è nessuno continuo dopo 5s la cancello
			let utentiRimastiInVC = oldState.channel.members
			if (utentiRimastiInVC.size === 0) {
				await wait (5 * 1000)
				utentiRimastiInVC = oldState.channel.members
				if (utentiRimastiInVC.size === 0) {
					oldState.channel.delete('canale vocale eliminato automaticamente perché vuoto')
				}
			}
			
		}
	},
};
	
