const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, GatewayIntentBits } = require('discord.js');
const { Player } = require('discord-player');
const { token } = require('./config/config.json');

// gli intent sono i "permessi" che il nostro bot ha, cosa puo' leggere dal server/dagli utenti
// https://discord-api-types.dev/api/discord-api-types-v10/enum/GatewayIntentBits
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildVoiceStates,
	GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMembers] });

// il player è per la chat vocale e la musica
// ha bisogno degli estrattori, che in sostanza estraggono musica dalle piattaforme e la sparano qui dentro
const player = new Player(client);

// noi gli facciamo scrivere la canzone in chat
player.events.on('playerStart', (queue, track) => {
    queue.metadata.channel.send(`Started playing **${track.title}**!`);
});
player.events.on('playerError', (queue, error) => {
  queue.metadata.channel.send(`C'è stato un errore, kickami dalla VC\n\n${error}`);
	// queue.metadata.channel.send('C\'è stato un errore, boooh!\nProvo a risolvere da solo\nNel caso kickami dalla VC');
	queue.delete();
});


// creiamo una Collezione che contiene tutti i nostri comandi, così quando il bot li riceve sa che file leggere
client.commands = new Collection();
client.cooldowns = new Collection();

const foldersPath = path.join(__dirname, 'commands');
const commandFolders = fs.readdirSync(foldersPath);

for (const folder of commandFolders) {
	const commandsPath = path.join(foldersPath, folder);
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file);
		const command = require(filePath);
		if ('data' in command && 'execute' in command) {
			client.commands.set(command.data.name, command);
		} else {
			console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

// registriamo gli eventi del bot
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
	const filePath = path.join(eventsPath, file);
	const event = require(filePath);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}

// facciamo fare il login del bot con discord
client.login(token);
