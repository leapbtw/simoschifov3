# simoschifoV3
simoschifo è un Bot per Discord, basato su [discord-player](https://github.com/androz2091/discord-player). 
Il bot ha runtime nodejs v20 LTS, ed usa [discord.js](https://discord.js.org/). 

Il nome del bot è ispirato al nostro amico Simone a cui vogliamo tanto bene

## Installazione bot per esecuzione 24/7
- crea un Applicazione sul [Developer Portal](https://discord.com/developers/applications/)
- per invitare il bot nel tuo server, dal Developer Portal genera un URL nella sezione **OAuth2** con gli scopes **bot** e **applications.commands**
- clona la repo ```git clone https://gitlab.com/leapbtw/simoschifo.git && cd simoschifo```
- configura il bot [CONFIG.md](./CONFIG.md)
- installa [docker](https://docker.com)
- entra nella cartella **simoschifo/docker**
- scegli come installare il bot: la configurazione di default usa un'immagine statica di **ffmpeg** per ridurre lo spazio disco occupato dall'immagine docker, ma c'è anche un ```Dockerfile``` che usa **ffmpeg** installato con **apt**
- ```./build_docker && ./run_docker``` (verrà chiesto se attivare o no il riavvio automatico del container).



## Installazione per programmatori (per modificare il bot)
Il bot è fatto per girare su distribuzioni linux Debian-like
- esegui gli stessi passaggi descritti sopra, ma fermati prima di installare docker
- aggiorna il sistema ```apt update && apt upgrade -y```
- installa [node.js v20](https://github.com/nodesource/distributions#debian-versions)
- installa le dipendenze con ```npm install```
- invia a discord la lista dei comandi del bot con ```node deploy-commands.js```
- esegui il bot con ```node .```

