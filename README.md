# simoschifo V4
**simoschifo** is a Discord Bot, based on [discord-player](https://github.com/androz2091/discord-player). 
The bot runs on nodejs v20 LTS and uses [discord.js](https://discord.js.org/).

The bot's name is inspired by our friend Simone, whom we love very much.

## How to run the bot 24/7
- Create an Application on the [Developer Portal](https://discord.com/developers/applications/)
- To invite the bot to your server, generate a URL in the **OAuth2** section of the Developer Portal with the **bot** and **applications.commands** scopes.
- Clone the repo: `git clone --depth 1 --recursive https://gitlab.com/leapbtw/simoschifo.git && cd simoschifo`
- Configure the bot [CONFIG.md](./CONFIG.md)
- Install [docker](https://docker.com)
- Run `docker buildx build -f docker/Dockerfile --tag simoschifo-v4 .` in the repo's root directory
- Run the bot with `docker run -d --name simoschifov4 --restart always simoschifo-v4`.
