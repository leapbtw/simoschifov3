# Configuration

- Create the folder **simoschifo/config**
- In this folder, create three files: **channels.json**, **config.json**, and **vc_names.txt**

### channels.json
Here you should enter the `channel_id` of the voice channels that the bot will use instead of comments, within the ""
```
{
    "CREATE_VC_CHANNEL_ID" : "if you enter this channel, the bot will create a new voice channel and move you into it",
    "CREATE_VC_CATEGORY_ID" : "the id of the CREATE_VC_CATEGORY_ID category",
    "repairs" : "8kbps voice channel, we use it when our internet is not working",
    "afk" : "afk voice channel"
}
```

### config.json
The main configuration file for the bot
```
{
    "token" : "the bot's token, you can find it on the Developer Portal",
    "clientId" : "the bot's user id",
    "guildId" : "discord server id"
}
```

### vc_names.txt
This file contains random words that the bot will select randomly to name the voice channels it creates.
Put random words separated by spaces
`Lamp Rain Guitar Mountain Monkey Horizon Pineapple Chocolate Bicycle`

