#!/bin/bash

# Ask the user a yes/no question
read -p "Do you want --restart=always ? [yn]" response

# Check the user's response
if [ "$response" == "y" ]; then
    echo ""
    docker run -d --restart always --name simoschifo leapbtw/simoschifo:1.0 
else
    echo "Performing action B..."
    docker run -d --name simoschifo leapbtw/simoschifo:1.0
fi
