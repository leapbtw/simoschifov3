This docker image uses a custom static binary of `ffmpeg`, this is because APT's ffmpeg has a LOT of dependencies that we don't even
need to run this bot. This saves ~500MB which might be a lot for people running this bot off something like a Raspberry Pi
with some 8GB SD card or stuff like that. The bot should be able to run basically any audio file, but I only tested `mp3` and `m4a`.

To build the docker image just follow the instructions in the [README](../README.md).

### IMPORTANT
As of writing this (16/9/24) the opus library used for arm64 (`@discordjs/opus`) is marked by `npm` as using deprecated 
and/or dependencies with vulnerabilities.
This being said, this was the only library i could get working on arm64, and also shouldn't affect the bot too much as it runs
in a docker container and i also suppose that only people in the discord server could SOMEHOW exploit those vulnerabilities. 
Therefore, if these issues with `@discordjs/opus` affect your threat model, I recommend considering to run simoschifo on an x86_64
machine instead as it then would instead use the `mediaplex` library.
