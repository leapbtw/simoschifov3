cd ffmpeg
./configure --disable-doc --disable-ffplay --disable-ffprobe --enable-libmp3lame --enable-libshine --enable-libopus --enable-static --disable-amf --disable-audiotoolbox --disable-cuda-llvm --disable-cuvid --disable-d3d11va --disable-d3d12va --disable-dxva2 --disable-ffnvcodec --disable-libdrm --disable-nvdec --disable-nvenc --disable-v4l2-m2m --disable-vaapi --disable-vdpau --disable-videotoolbox --disable-gnutls --disable-libtls --disable-mbedtls --disable-sndio --disable-schannel --disable-sdl2 --disable-securetransport --disable-xlib --disable-zlib --disable-alsa --disable-appkit --disable-avfoundation --disable-bzlib --disable-coreimage --disable-iconv \
 --disable-decoder=$(cat ../list_decoders) --disable-encoder=$(cat ../list_encoders) --disable-muxer=$(cat ../list_muxers) --disable-indevs --disable-outdevs --disable-demuxer=$(cat ../list_demuxers) --disable-bsf=$(cat ../list_bsfs) \
 --disable-shared --enable-small --pkg-config-flags="--static" --extra-ldflags="-static" --extra-libs="-lpthread -lm"

make -j$(nproc)

# --disable-filter=$(cat list_filters) --disable-protocol=$(cat list_protos)

